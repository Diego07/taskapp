package taskapp.repository;

import org.springframework.data.repository.CrudRepository;
import taskapp.domain.Task;

public interface TaskRepository extends CrudRepository<Task, Long> {
}
