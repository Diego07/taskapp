package taskapp.service;

import org.springframework.stereotype.Service;
import taskapp.domain.Task;


public interface TaskService {

    Iterable<Task> list();

    Task save(Task task);
}
