package taskapp;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import taskapp.domain.Task;
import taskapp.service.TaskService;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@SpringBootApplication
public class TaskappApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskappApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(TaskService taskService) {

		return args -> {
			taskService.save(new Task(1L,"Create Spring Boot app", LocalDate.now(), true));
			taskService.save(new Task(2L, "Create Spring Boot packages", LocalDate.now().
					plus(1, ChronoUnit.DAYS), false));
			taskService.save(new Task(3L, "Create Task domain Class", LocalDate.now()
					.plus(3,ChronoUnit.DAYS), false));
			taskService.save(new Task(4L, "Create service and repo calsses", LocalDate.now()
					.plus(5, ChronoUnit.DAYS), false));
			taskService.save(new Task(5L, "Create the command line runner to load data", LocalDate.now()
					.plus(8, ChronoUnit.DAYS), false));
			taskService.save(new Task(6L, "Create the required configuration properties", LocalDate.now()
					.plus(10, ChronoUnit.DAYS), false));
			taskService.save(new Task(7L, "Run the Spring Boot App", LocalDate.now()
					.plus(12, ChronoUnit.DAYS), false));
			taskService.save((new Task(8L, "Chec the H2 console for the initial data", LocalDate.now()
					.plus(13, ChronoUnit.DAYS), false)));
		};

	}
}
