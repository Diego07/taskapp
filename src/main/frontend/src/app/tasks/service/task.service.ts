import {HttpClient} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Task} from '../model/task.model';
import {Observable} from 'rxjs';


@Injectable()
export class TaskService {

    onTaskAdded = new EventEmitter<Task>();

    constructor(private http: HttpClient) {
    }

    getTasks() {
        return this.http.get<Task[]>('/api/tasks');
    }

    saveTask(task: Task, checked: boolean) {
        task.completed = checked;
        return this.http.post<Task>('api/tasks/save', task);
    }

    addTask(task: Task) {
        return this.http.post<Task>('api/tasks/save', task);
    }

}

