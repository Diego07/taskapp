import { Component, OnInit } from '@angular/core';
import {Task} from '../model/task.model';
import {TaskService} from '../service/task.service';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {

    tasks: Task[] = [];

  constructor(private taskService: TaskService) { }

  ngOnInit() {
      this.loadTasks();
      this.addTask();
  }

  loadTasks() {
      return this.taskService.getTasks()
          .subscribe((tasks) => {
              this.tasks = tasks;
          });
  }

  addTask() {
      this.taskService.onTaskAdded.subscribe(
          (task: Task) => this.tasks.push(task)
      );
  }


  getDueDateLabel(task: Task) {
      return task.completed ? 'alert-success' : 'alert-info';
  }
  onTaskChange(event, task) {
      this.taskService.saveTask(task, event.target.checked).subscribe();
  }

}
